package io.inapinch.reports.simple

import io.inapinch.reports.Request

data class Hello(val name: String) : Request {
    override val templatePath: String = "hello"
}