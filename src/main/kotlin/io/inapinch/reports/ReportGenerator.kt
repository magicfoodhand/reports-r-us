package io.inapinch.reports

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.thymeleaf.TemplateEngine
import org.thymeleaf.TemplateSpec
import org.thymeleaf.context.Context
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver
import org.xhtmlrenderer.pdf.ITextRenderer
import java.io.File
import java.io.FileOutputStream
import java.util.*


interface ReportGenerator {
    fun html(templateName: String, context: Map<String, Any>) : String

    fun pdf(html: String) : File

    fun pdf(templateName: String, context: Map<String, Any>) : File = pdf(html(templateName, context))
}

private fun File.cleanup() {
    this.deleteOnExit()
    GlobalScope.launch {
        delay(60_000)
        this@cleanup.delete()
    }
}

class FlyleafGenerator private constructor(
    private val templateEngine: TemplateEngine
) : ReportGenerator {
    override fun html(templateName: String, context: Map<String, Any>) : String {
        val binding = Context()
        binding.setVariables(context)
        return templateEngine.process(templateName, binding)
    }

    override fun pdf(html: String) : File {
        val filename = "${UUID.randomUUID()}.pdf"
        FileOutputStream(filename).use { outputStream ->
            val renderer = ITextRenderer()
            renderer.setDocumentFromString(html)
            renderer.layout()
            renderer.createPDF(outputStream)
        }

        return File(filename).also(File::cleanup)
    }

    companion object {
        fun create() : FlyleafGenerator {
            val templateResolver = ClassLoaderTemplateResolver()
            templateResolver.suffix = ".html"
            templateResolver.setTemplateMode("HTML")

            val templateEngine = TemplateEngine()
            templateEngine.setTemplateResolver(templateResolver)
            return FlyleafGenerator(templateEngine)
        }
    }
}