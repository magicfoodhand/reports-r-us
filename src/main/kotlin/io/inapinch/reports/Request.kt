package io.inapinch.reports

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import io.inapinch.reports.invoices.Invoice
import io.inapinch.reports.simple.Hello

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "@type")
@JsonSubTypes(value = [
    JsonSubTypes.Type(Invoice::class),
    JsonSubTypes.Type(Hello::class)
])
interface Request {
    @get:JsonIgnore
    val templatePath: String
}