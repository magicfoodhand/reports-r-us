package io.inapinch.reports.invoices

data class Address(val line1: String, val line2: String?, val city: String, val state: String, val zip: Int)

data class Client(val name: String, val address: Address)