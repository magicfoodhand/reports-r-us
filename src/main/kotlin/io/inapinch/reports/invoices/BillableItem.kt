package io.inapinch.reports.invoices

data class BillableItem(val quantity: Double, val amount: Double, val description: String)