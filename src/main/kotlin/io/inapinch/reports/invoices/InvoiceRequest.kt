package io.inapinch.reports.invoices

import io.inapinch.reports.Request

data class Invoice(
    val invoiceNumber: String,
    val client: Client,
    val billableItems: List<BillableItem>,
    val notes: String?,
    val total: Double
) : Request {

    override val templatePath: String = "invoice"
}