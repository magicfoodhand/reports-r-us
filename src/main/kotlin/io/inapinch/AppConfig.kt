package io.inapinch

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import io.inapinch.reports.FlyleafGenerator
import io.inapinch.reports.ReportGenerator
import org.glassfish.hk2.utilities.binding.AbstractBinder
import org.glassfish.jersey.server.ResourceConfig
import javax.ws.rs.ApplicationPath

@ApplicationPath("/api")
class AppConfig : ResourceConfig() {
    init {
        packages("io.inapinch.server.resolvers", "io.inapinch.api")
        register(AppBinder())
        register(JacksonJaxbJsonProvider::class.java)
    }
}

private class AppBinder : AbstractBinder() {
    override fun configure() {
        bind(objectMapper()).to(ObjectMapper::class.java)
        bind(FlyleafGenerator.create()).to(ReportGenerator::class.java)
    }

    private fun objectMapper() : ObjectMapper = jacksonObjectMapper()
            .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .registerModule(JavaTimeModule())
}
