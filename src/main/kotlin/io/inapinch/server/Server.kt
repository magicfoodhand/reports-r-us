package io.inapinch.server

import io.inapinch.AppConfig
import io.undertow.Handlers
import io.undertow.Undertow
import io.undertow.server.handlers.resource.ClassPathResourceManager
import io.undertow.servlet.Servlets
import io.undertow.servlet.api.DeploymentManager
import org.glassfish.jersey.servlet.ServletContainer
import org.slf4j.LoggerFactory

object Server {
    private val LOG = LoggerFactory.getLogger(Server.javaClass)
    private lateinit var server: Undertow
    private lateinit var deploymentManager: DeploymentManager

    /**
     * Start server on the given port.
     *
     * @param port
     */
    fun start(port: Int) {
        val path = Handlers.path()

        server = Undertow.builder()
            .addHttpListener(port, "localhost")
            .setHandler(path)
            .build()
            .also(Undertow::start)

        LOG.info("Server started on port {}", port)

        Servlets.deployment()
            .setClassLoader(Server.javaClass.classLoader)
            .setContextPath("/")
            .setResourceManager(ClassPathResourceManager(Server.javaClass.classLoader))
            .addServlets(
                Servlets.servlet("jerseyServlet", ServletContainer::class.java)
                    .setLoadOnStartup(1)
                    .addInitParam("javax.ws.rs.Application", AppConfig::class.java.name)
                    .addMapping("/api/*")
            )
            .setDeploymentName("application.war").also { deploymentInfo ->
                deploymentManager = Servlets.defaultContainer()
                    .addDeployment(deploymentInfo)
                    .also(DeploymentManager::deploy)

                path.addPrefixPath("/", deploymentManager.start())
            }

        LOG.info("Application started")
    }

    /**
     * Stop server.
     */
    fun stop() {
        deploymentManager.undeploy()
        server.stop()

        LOG.error("Application stopped")
    }
}
