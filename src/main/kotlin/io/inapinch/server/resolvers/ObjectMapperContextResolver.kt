package io.inapinch.server.resolvers

import com.fasterxml.jackson.databind.ObjectMapper
import javax.inject.Inject
import javax.inject.Singleton
import javax.ws.rs.ext.ContextResolver
import javax.ws.rs.ext.Provider

// Used by JacksonJaxbJsonProvider
@Provider
@Singleton
class ObjectMapperContextResolver
@Inject constructor(
    private val mapper: ObjectMapper
) : ContextResolver<ObjectMapper> {

    override fun getContext(type: Class<*>) = mapper
}