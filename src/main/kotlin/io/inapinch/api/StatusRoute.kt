package io.inapinch.api

import java.time.LocalDateTime
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType

@Path("status")
@Produces(MediaType.APPLICATION_JSON)
class StatusRoute  {
    @GET
    fun status() = StatusResult()

    data class StatusResult(val timestamp: LocalDateTime = LocalDateTime.now())
}