package io.inapinch.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.convertValue
import io.inapinch.reports.ReportGenerator
import io.inapinch.reports.Request
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.Context
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response
import javax.ws.rs.core.UriInfo

private val UNSUPPORTED_CONTENT_TYPE = Response.Status.fromStatusCode(406)

@Path("v1/reports")
class ReportGeneratorRoute @Inject constructor(
    private val reportGenerator: ReportGenerator,
    private val objectMapper: ObjectMapper
) {
    @GET
    @Path("{name}")
    @Produces(MediaType.TEXT_HTML)
    fun getHtml(
        @Context uriInfo: UriInfo,
        @PathParam("name") templateName: String
    ) = reportGenerator.html(templateName, uriInfo.queryParameters)

    @GET
    @Path("{name}/pdf")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    fun getPdf(
        @Context uriInfo: UriInfo,
        @PathParam("name") templateName: String
    ) = reportGenerator.pdf(templateName, uriInfo.queryParameters)

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML, MediaType.APPLICATION_OCTET_STREAM)
    fun generatePost(
        request: Request,
        @HeaderParam("Accepts") contentType: String
    ) = generate(request, contentType)

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML, MediaType.APPLICATION_OCTET_STREAM)
    fun generatePut(
        request: Request,
        @HeaderParam("Accepts") contentType: String
    ) = generate(request, contentType)

    private fun Request.toContext() : Map<String, Any> = this.run(objectMapper::convertValue)

    private fun generate(
        request: Request,
        contentType: String
    ) : Response {
        return when(contentType) {
            MediaType.APPLICATION_OCTET_STREAM ->
                Response.ok(reportGenerator.pdf(request.templatePath, request.toContext()))
            MediaType.TEXT_HTML ->
                Response.ok(reportGenerator.html(request.templatePath, request.toContext()))
            else ->
                Response.status(UNSUPPORTED_CONTENT_TYPE).entity("$contentType unsupported")
        }.build()
    }
}