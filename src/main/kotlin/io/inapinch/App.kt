package io.inapinch

import io.inapinch.server.Server

fun main(args: Array<String>) {
    val port = System.getenv("PORT")?.toInt() ?: 8080
    Server.start(port)
}
